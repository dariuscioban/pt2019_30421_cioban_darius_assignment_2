package clients;

public class Client {
	
	private int waitingTime;
	private int queueTime;
	
	public Client(int waitingTime, int queueTime) {
		this.waitingTime = waitingTime;
		this.queueTime = queueTime;
	}
	
	public int getWaitingTime() {
		return waitingTime;
	}

	public int getQueueTime() {
		return queueTime;
	}
	
	public void decrementWaitingTime() {
		waitingTime--;
	}
	
	public void decrementQueueTime() {
		queueTime--;
	}
}
