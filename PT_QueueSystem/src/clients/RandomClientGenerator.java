package clients;
import java.util.*;

public class RandomClientGenerator {

	private int minWaitingTime;
	private int maxWaitingTime;
	private int minQueueTime;
	private int maxQueueTime;
	
	public RandomClientGenerator(int minWaitingTime, int maxWaitingTime, int minQueueTime, int maxQueueTime) {
		this.minWaitingTime = minWaitingTime;
		this.maxWaitingTime = maxWaitingTime;
		this.minQueueTime = minQueueTime;
		this.maxQueueTime = maxQueueTime;
	}
	
	public Client generateClient() {
		Random randNum = new Random();
		int waitingTime = randNum.nextInt(maxWaitingTime + 1 - minWaitingTime) + minWaitingTime;
		int queueTime = randNum.nextInt(maxQueueTime + 1 - minQueueTime) + minQueueTime;
		return new Client(waitingTime, queueTime);
	}
	
	
}
