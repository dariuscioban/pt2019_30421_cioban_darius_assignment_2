package simulation;
import java.util.*;
import clients.*;

public class Queue implements Runnable {

	private List<Client> clientQueue = Collections.synchronizedList(new ArrayList<Client>());
	private EventsLog log;
	private QueueManager manager;
	private boolean active;
	private int totalWaitingTime;
	private int totalClientsNumber;
	private int emptyQueueTime;
	private int waitingTime;
	private int id;
	
	public Queue(int id, EventsLog log, QueueManager manager) {
		this.id = id;
		active = false;
		totalWaitingTime = 0;
		totalClientsNumber = 0;
		emptyQueueTime = 0;
		this.log = log;
		this.manager = manager;
	}
	
	public synchronized void run() {
		waitingTime = 0;
		while(true) {
			while(active) {
				if(!clientQueue.isEmpty()) {
					decreaseTime();
					waitingTime++;
				} else {
					emptyQueueTime++;
				}
				try {
					Thread.sleep(1000);
				}
				catch(Exception e) {
					//exception not treated
				}
			}
		}
	}
	
	public int getSize() {
		return clientQueue.size();
	}
	
	public float getAverageWaitingTime() {
		if(totalClientsNumber == 0)
			return 0;
		return (float) totalWaitingTime / totalClientsNumber;
	}
	
	public int getEmptyQueueTime() {
		return emptyQueueTime;
	}
	
	private void decreaseTime() {
		clientQueue.get(0).decrementQueueTime();
		if(clientQueue.get(0).getQueueTime() <= 0) {
			removeClient();
			totalClientsNumber++;
			totalWaitingTime += waitingTime;
			waitingTime = 0;
			log.clientLeft(manager.getSimulationTime(), id);
		}
	}
	
	public boolean addClient(Client client) {
		if(active) {
			clientQueue.add(client);
			log.clientAdded(manager.getSimulationTime(), id);
			return true;
		}
		return false;
	}
	
	private void removeClient() {
		clientQueue.remove(0);
	}
	
	public void toggleActive() {
		active = !active;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public int getId() {
		return id;
	}
}
