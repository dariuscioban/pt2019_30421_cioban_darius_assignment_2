package simulation;
import java.util.*;
import clients.*;

public class QueueManager implements Runnable{

	private int noOfQueues;
	public Queue[] queues;
	private EventsLog log;
	private int clientMinSpawnTime;
	private int clientMaxSpawnTime;
	private int clientMinWaitingTime;
	private int clientMaxWaitingTime;
	private int clientMinQueueTime;
	private int clientMaxQueueTime;
	
	private volatile int simulationTime;
	private volatile float averageWaitingTime;
	private volatile int peakHourTime;
	private volatile int peakHourClients;
	private volatile float averageEmptyQueueTime;
	
	public QueueManager(int noOfQueues, int clientMinSpawnTime, int clientMaxSpawnTime,
			int clientMinWaitingTime, int clientMaxWaitingTime, int clientMinQueueTime, int clientMaxQueueTime, EventsLog log) {
		this.noOfQueues = noOfQueues;
		queues = new Queue[noOfQueues];
		this.clientMinSpawnTime = clientMinSpawnTime;
		this.clientMaxSpawnTime = clientMaxSpawnTime;
		this.clientMinWaitingTime = clientMinWaitingTime;
		this.clientMaxWaitingTime = clientMaxWaitingTime;
		this.clientMinQueueTime = clientMinQueueTime;
		this.clientMaxQueueTime = clientMaxQueueTime;
		simulationTime = 0;
		averageWaitingTime = 0.0f;
		averageEmptyQueueTime = 0.0f;
		peakHourTime = 0;
		peakHourClients = 0;
		this.log = log;
	}

	public void run() {
		int timeToGenerateClient = 0;
		List<Client> clientList = new ArrayList<Client>();
		initializeQueues();
		activateAllQueues();
		Thread[] threads = createThreads();
		for(int i = 0; i < noOfQueues; i++)
			threads[i].start();
		RandomClientGenerator clientGen = new RandomClientGenerator(clientMinWaitingTime, clientMaxWaitingTime, clientMinQueueTime, clientMaxQueueTime);
		while(true) {
			timeToGenerateClient--;
			if(timeToGenerateClient <= 0) {
				clientList.add(clientGen.generateClient());
				log.clientGenerated(simulationTime, clientList.get(clientList.size() - 1).getWaitingTime(), clientList.get(clientList.size() - 1).getQueueTime());
				timeToGenerateClient = getNewSpawnTime();
			}
			decreaseClientWaitingTime(clientList);
			for(int i = 0; i < clientList.size(); i++) {
				if(clientList.get(i).getWaitingTime() < 0 && addToSmallestQueue(clientList.get(i))) 
					clientList.remove(i);
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				//exception not treated
			}
			updateAverageWaitingTime();
			updateAverageEmptyQueueTime();
			updatePeakHour();
			simulationTime++;
		}
	}
	
	public int getSimulationTime() {
		return simulationTime;
	}
	
	private int getNewSpawnTime() {
		Random randNum = new Random();
		return randNum.nextInt(clientMaxSpawnTime - clientMinSpawnTime + 1) + clientMinSpawnTime;
	}
	
	private void initializeQueues() {
		for(int i = 0; i < noOfQueues; i++) 
			queues[i] = new Queue(i + 1, log, this);
	}
	
	private Thread[] createThreads() {
		Thread[] threads = new Thread[noOfQueues];
		for(int i = 0; i < noOfQueues; i++)
			threads[i] = new Thread(queues[i]);
		return threads;
	}
	
	private boolean addToSmallestQueue(Client c) {
		Queue minQueue = new Queue(0, log, this);
		int minQueueSize = Integer.MAX_VALUE;
		for(int i = 0; i < noOfQueues; i++) {
			if(queues[i].getSize() < minQueueSize && queues[i].isActive()) {
				minQueue = queues[i];
				minQueueSize = minQueue.getSize();
			}
		}
		if(minQueueSize == Integer.MAX_VALUE)
			return false;
		minQueue.addClient(c);
		return true;
	}
	
	private void decreaseClientWaitingTime(List<Client> clients) {
		for(Client c: clients) {
			c.decrementWaitingTime();
		}
	}
	
	public void activateAllQueues() {
		for(int i = 0; i< noOfQueues; i++) {
			queues[i].toggleActive();
		}
	}
	
	private void updatePeakHour() {
		int curClients = 0;
		for(Queue q: queues) {
			curClients += q.getSize();
		}
		if(curClients > peakHourClients) {
			peakHourClients = curClients;
			peakHourTime = simulationTime;
		}
	}
	
	public int[] getPeakHour() {
		int[] peakHour = new int[2];
		peakHour[0] = peakHourTime;
		peakHour[1] = peakHourClients;
		return peakHour;
	}
	
	private void updateAverageWaitingTime() {
		float increment = 0.0f;
		for(Queue q: queues) {
			increment += q.getAverageWaitingTime();
		}
		averageWaitingTime = increment / noOfQueues;
	}
	
	public float getAverageWaitingTime() {
		return averageWaitingTime;
	}
	
	private void updateAverageEmptyQueueTime() {
		float increment = 0.0f;
		for(Queue q: queues) {
			increment += q.getEmptyQueueTime();
		}
		averageEmptyQueueTime = increment / noOfQueues;
	}
	
	public float getAverageEmptyQueueTime() {
		return averageEmptyQueueTime;
	}
	
	public int[] getQueueSizes() {
		int[] result = new int[noOfQueues];
		for(int i = 0; i < noOfQueues; i++) {
			result[i] = queues[i].getSize();
		}
		return result;
	}
}
