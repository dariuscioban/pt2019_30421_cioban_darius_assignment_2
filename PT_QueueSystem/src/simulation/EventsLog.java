package simulation;
import java.util.*;

public class EventsLog {
	private List<String> log;
	
	public EventsLog() {
		log = new ArrayList<String>();
	}
	
	public void clientGenerated(int simTime, int waitingTime, int queueTime) {
		String logData = timeInMinutes(simTime) + " : Client generated with waiting time " + waitingTime + " and queue time " + queueTime + ".";
		log.add(logData);
	}
	
	public void clientAdded(int simTime, int queueId) {
		String logData = timeInMinutes(simTime) + " : Client added in queue " + queueId + ".";
		log.add(logData);
	}
	
	public void clientLeft(int simTime, int queueId) {
		String logData = timeInMinutes(simTime) + " : Client left queue " + queueId + ".";
		log.add(logData);
	}
	
	public List<String> getLog() {
		return log;
	}
	
	public String timeInMinutes(int time) {
		int min = time / 60;
		int sec = time % 60;
		String minStr = "";
		String secStr = "";
		
		if(min / 10 == 0) {
			minStr = "0" + min;
		} else
			minStr = Integer.toString(min);
		if(sec / 10 == 0) {
			secStr = "0" + sec;
		} else
			secStr = Integer.toString(sec);
		return minStr + ":" + secStr;
	}
}
