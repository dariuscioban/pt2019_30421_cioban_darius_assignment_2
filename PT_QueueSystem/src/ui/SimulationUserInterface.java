package ui;
import javax.swing.*;
import java.util.*;
import simulation.*;

public class SimulationUserInterface implements Runnable {
	
	EventsLog log;
	QueueManager manager;
	
	JLabel[] queueId;
	JLabel[] queueSize;
	JLabel avgWaitTime;
	JLabel peakHourTime;
	JLabel peakHourSize;
	JLabel avgEmptyTime;
	JLabel simTime;
	JTextArea logArea;
	
	
	JFrame simulationFrame = new JFrame("Simulation Running");
	
	public SimulationUserInterface(int noOfQueues, int minClientSpawnTime, int maxClientSpawnTime, int minClientWaitingTime,
			int maxClientWaitingTime, int minClientQueueTime, int maxClientQueueTime) {
		log = new EventsLog();
		manager = new QueueManager(noOfQueues, minClientSpawnTime, maxClientSpawnTime, minClientWaitingTime, 
				maxClientWaitingTime, minClientQueueTime, maxClientQueueTime, log);
		queueId = new JLabel[10];
		queueSize = new JLabel[10];
		for(int i = 0; i < 10; i++) {
			queueId[i] = new JLabel();
			queueSize[i] = new JLabel();
			queueId[i].setText("Queue " + Integer.toString(i + 1) + " :");
			if(i < noOfQueues)
				queueSize[i].setText(" 0");
			else queueSize[i].setText("not used");
		}
		avgWaitTime = new JLabel("0");
		peakHourTime = new JLabel("00:00");
		peakHourSize = new JLabel("0");
		avgEmptyTime = new JLabel("0");
		simTime = new JLabel("00:00");
		logArea = new JTextArea(30, 25);
	}
	
	private void initUI() {
		simulationFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		simulationFrame.setSize(800, 600);

		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
		JPanel queuePanel = new JPanel();
		queuePanel.setLayout(new BoxLayout(queuePanel, BoxLayout.Y_AXIS));
		JPanel[] queueDataPanel = new JPanel[10];
		JPanel infoPanel = new JPanel();
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
		JPanel[] infoPanelParts = new JPanel[6];
		for(int i = 0; i < 6; i++) {
			infoPanelParts[i] = new JPanel();
		}
		JPanel logPanel = new JPanel();
		
		for(int i = 0; i < 10; i++) {
			queueDataPanel[i] = new JPanel();
			queueDataPanel[i].add(queueId[i]);
			queueDataPanel[i].add(queueSize[i]);
			queuePanel.add(queueDataPanel[i]);
		}
		JLabel avgWaitTimeLabel = new JLabel("Average waiting time: ");
		JLabel peakHourTimeLabel = new JLabel("Peak hour: ");
		JLabel peakHourSizeLabel = new JLabel("Clients during peak hour: ");
		JLabel avgEmptyTimeLabel = new JLabel("Average empty queue time: ");
		JLabel simTimeLabel = new JLabel("Current simulation time: ");
		JLabel spaceLabel = new JLabel("	");
	    logArea.setEditable(false);
	    JScrollPane scroll = new JScrollPane(logArea);
	    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	    
		logPanel.add(scroll);
		infoPanelParts[0].add(avgWaitTimeLabel);
		infoPanelParts[1].add(peakHourTimeLabel);
		infoPanelParts[2].add(peakHourSizeLabel);
		infoPanelParts[3].add(avgEmptyTimeLabel);
		infoPanelParts[4].add(spaceLabel);
		infoPanelParts[5].add(simTimeLabel);
		infoPanelParts[0].add(avgWaitTime);
		infoPanelParts[1].add(peakHourTime);
		infoPanelParts[2].add(peakHourSize);
		infoPanelParts[3].add(avgEmptyTime);
		infoPanelParts[4].add(spaceLabel);
		infoPanelParts[5].add(simTime);
		for(int i = 0; i < 6; i++) {
			infoPanel.add(infoPanelParts[i]);
		}
		mainPanel.add(queuePanel);
		mainPanel.add(infoPanel);
		mainPanel.add(logPanel);
		
		simulationFrame.setContentPane(mainPanel);
		simulationFrame.setVisible(true);
	}
	
	private void runSimulation() {
		Thread managerThread = new Thread(manager);
		managerThread.start();
	}
	
	private void updateInfo() {
		avgWaitTime.setText(Float.toString(manager.getAverageWaitingTime()));
		peakHourTime.setText(log.timeInMinutes(manager.getPeakHour()[0]));
		peakHourSize.setText(Float.toString(manager.getPeakHour()[1]));
		avgEmptyTime.setText(Float.toString(manager.getAverageEmptyQueueTime()));
		simTime.setText(log.timeInMinutes(manager.getSimulationTime()));
		for(int i = 0; i < 10; i++) {
			if(queueSize[i].getText() != "not used")
				queueSize[i].setText(Integer.toString(manager.getQueueSizes()[i]));
		}
	}
	
	private void updateLog() {
		logArea.setText("");
		List<String> text = log.getLog();
		for(int i = 0; i < text.size(); i++) {
			logArea.append(text.get(i));
			logArea.append("\n");
		}
	}

	public void run() {
		initUI();
		runSimulation();
		while(true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				//exception not treated
			}
			updateInfo();
			updateLog();
		}
	}
}
