package ui;
import java.awt.event.*;
import javax.swing.*;

public class SetupUserInterface {

	JFrame setupFrame = new JFrame("Simulation Mananger");

	JTextField minSpawnField = new JTextField("	");
	JTextField maxSpawnField = new JTextField("	");
	JTextField minWaitField = new JTextField("	");
	JTextField maxWaitField = new JTextField("	");
	JTextField minQueueField = new JTextField("	");
	JTextField maxQueueField = new JTextField("	");
	JTextField noOfQueuesField = new JTextField("	");

	int noOfQueues;
	int minClientSpawnTime;
	int maxClientSpawnTime;
	int minClientWaitingTime;
	int maxClientWaitingTime;
	int minClientQueueTime;
	int maxClientQueueTime;

	void initSetupUI() {
		setupFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setupFrame.setSize(600, 300);

		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		JPanel panel4 = new JPanel();

		JLabel minSpawnLabel = new JLabel("Min spawn time:");
		JLabel maxSpawnLabel = new JLabel("Max spawn time:");
		JLabel minWaitLabel = new JLabel("Min waiting time:");
		JLabel maxWaitLabel = new JLabel("Max waiting time:");
		JLabel minQueueLabel = new JLabel("Min queue time:");
		JLabel maxQueueLabel = new JLabel("Max queue time:");
		JLabel noOfQueuesLabel = new JLabel("Number of queues:");
		JButton runBtn = new JButton("Run Simulation");

		runBtn.addActionListener(new RunButtonClick());

		panel1.add(minSpawnLabel);
		panel1.add(minSpawnField);
		panel1.add(maxSpawnLabel);
		panel1.add(maxSpawnField);
		panel2.add(minWaitLabel);
		panel2.add(minWaitField);
		panel2.add(maxWaitLabel);
		panel2.add(maxWaitField);
		panel3.add(minQueueLabel);
		panel3.add(minQueueField);
		panel3.add(maxQueueLabel);
		panel3.add(maxQueueField);
		panel4.add(noOfQueuesLabel);
		panel4.add(noOfQueuesField);
		mainPanel.add(panel1);
		mainPanel.add(panel2);
		mainPanel.add(panel3);
		mainPanel.add(panel4);
		mainPanel.add(runBtn);

		setupFrame.setContentPane(mainPanel);
		setupFrame.setVisible(true);
	}

	private class RunButtonClick implements ActionListener {


		public void actionPerformed(ActionEvent arg0) {
			JTextField[] fields = {minSpawnField, maxSpawnField, minWaitField, maxWaitField, minQueueField, maxQueueField, noOfQueuesField};
			if(!checkInput(fields))
				return;
			minClientSpawnTime = Integer.parseInt(minSpawnField.getText());
			maxClientSpawnTime = Integer.parseInt(maxSpawnField.getText());
			minClientWaitingTime = Integer.parseInt(minWaitField.getText());
			maxClientWaitingTime = Integer.parseInt(maxWaitField.getText());
			minClientQueueTime = Integer.parseInt(minQueueField.getText());
			maxClientQueueTime = Integer.parseInt(maxQueueField.getText());
			noOfQueues = Integer.parseInt(noOfQueuesField.getText());
			if(!checkRange())
				return;
			resetSetupFields();

			startSimulation();
		}
	}
	
	private void startSimulation() {
		SimulationUserInterface simUI = new SimulationUserInterface(noOfQueues, minClientSpawnTime, maxClientSpawnTime,
				minClientWaitingTime, maxClientQueueTime, minClientQueueTime, maxClientQueueTime);
		Thread simThread = new Thread(simUI);
		simThread.start();
	}

	private boolean checkRange() {
		boolean ok = true;
		if(minClientSpawnTime > maxClientSpawnTime)
			ok = false;
		if(minClientWaitingTime > maxClientWaitingTime)
			ok = false;
		if(minClientQueueTime > maxClientQueueTime)
			ok = false;
		if(noOfQueues > 10 || noOfQueues < 1)
			ok = false;
		if(ok)
			return true;
		else {
			JOptionPane.showMessageDialog(setupFrame, "The minium of a range cannot be larger than its maximum!\n Number of queues must be smaller than 10!", "Error!", JOptionPane.ERROR_MESSAGE);
    		return false;
		}
	}

	private boolean checkInput(JTextField[] fields) {
		boolean empty = false;
		for(int i = 0; i < fields.length; i++) {
			if (fields[i].getText().replaceAll("\\s", "").isEmpty())
				empty = true;
		}
    	if(empty) {
    		JOptionPane.showMessageDialog(setupFrame, "Must fill all of the fields!", "Error!", JOptionPane.ERROR_MESSAGE);
    		return false;
    	}
    	return true;
    }

	public static void main(String[] args) {
		SetupUserInterface mainUI = new SetupUserInterface();
		mainUI.initSetupUI();
		//this sleep is used to reset the fields without also shrinking them
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			//exception not treated
		}
		mainUI.resetSetupFields();

	}
	
	private void resetSetupFields() {
		minSpawnField.setText("");
		maxSpawnField.setText("");
		minWaitField.setText("");
		maxWaitField.setText("");
		minQueueField.setText("");
		maxQueueField.setText("");
		noOfQueuesField.setText("");
	}
}
